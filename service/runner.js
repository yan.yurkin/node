const onError = error => {
    if (error.syscall !== 'listen') {
        throw error;
    }
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error({'Some of ports require elevated privileges': error});
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error({'Already in use': error});
            process.exit(1);
            break;
        default:
            throw error;
    }
};

module.exports = config => () => {
    console.log(`Starting ${config.name} in ${process.env.NODE_ENV} mode`);

    const debug = require('debug')(config.name);
    const http = require('http');
    const service = require('./worker')(config);
    service.set('port', config.port);
    const httpService = http.createServer(service);
    httpService.listen(config.port);
    httpService.on('error', onError);
    httpService.on('listening', () => {
        const addr = httpService.address();
        debug(config.name+' is listening on ' + (typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port));
    });
};
