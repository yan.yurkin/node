/*****************************
 config = {
    port: 889988,
    name: 'camalog:service'|'wristband:qr', //some string id
    worker: service => {
        service.get('/some/url', (req, res, next)=>{
            res.send('ok')
        })
    },
}
 *****************************/
module.exports = config => ({
    config,
    interface: require('./interface')(config),
    run: require('./runner')(config),
});