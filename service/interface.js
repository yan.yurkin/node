const axios = require('axios');

module.exports = config => {
    const label = `${config.name} error`;
    const errorHandler = (error, fail) => {
        console.log({[label]: error});
        if (fail) throw error;
    };
    const host = config.host || 'localhost';
    return {
        get(path, failOnError = false) {
            if (!path) return Promise.reject("Please specify the path to get");
            return axios
                .get(`http://${host}:${config.port}${path}`)
                .then(response => response.data)
                .catch(e => errorHandler(e, failOnError));
        },
        post(path, data, failOnError = false) {
            if (!path) return Promise.reject("Please specify the path to post");
            return axios
                .post(`http://${host}:${config.port}${path}`, data)
                .then(response => response.data)
                .catch(e => errorHandler(e, failOnError));
        },
        delete(path, failOnError = false) {
            if (!path) return Promise.reject("Please specify the path to delete");
            return axios
                .delete(`http://${host}:${config.port}${path}`)
                .then(response => response.data)
                .catch(e => errorHandler(e, failOnError));
        },
    }
};