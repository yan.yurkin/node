require('../essentials');
const express = require('express');
const entitiesStripper = require('lifeNode/http/middleware/entitiesStripper');

module.exports = config => {
    const service = express();
    service.use(express.json({ limit: '3mb' }));
    service.use(express.urlencoded({extended: false}));
    service.use(entitiesStripper);

    config.worker(service);

    //the VERY last one - if none matched - 404
    service.all('*', function (req, res, next) {
        if(!res.headersSent) res.sendStatus(404);
    });
    return service;
};
