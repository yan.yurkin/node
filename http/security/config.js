const _ = require('lodash');
const express = require('express');
const Router = express.Router;
const passport = require('passport');
const db = require('lifeNode/data/db');
const Generator = require('../../data/util/Generator');

const User = db.models.User;

const middlewareChain = [
    (role, nextAuth) => (req, res, next) => {
        if (req.user && (
            !role ||
            (role && req.user.roles && (
                (_.isString(role) && req.user.roles.includes(role)) ||
                (_.isArray(role) && req.user.roles.find(user_role => role.includes(user_role)))
            ))
        )){
            return next();
        }
        req.logout();
        req.session.returnTo = req.originalUrl;
        res.status(401).redirect('/auth/login');
    },
];

module.exports = config => {
    return {
        config,
        roles: Object.keys(config.roles)
                    .filter(name => 'public' !== name)
                    .reduce((obj, name)=>({...obj, [name]:name}), {}),
        worker: app => {
            if (!config.auth) return;
            app.use(passport.initialize());
            app.use(passport.session());
            const authRouter = Router();
            let modules = config.auth.modules;
            if (!modules) return;
            Object.keys(modules).forEach(name => {
                const options = modules[name];
                const authModule = require(`lifeNode/http/security/auth/${name}`);
                const authModuleRouter = Router();
                const middlewareProvider = authModule.init(
                    passport,
                    options,
                    authModuleRouter,
                    {
                        async authOrCreateFirstUser(query, data, authenticate, userCreation) {
                            try {
                                let user;
                                const usersCount = await User.estimatedDocumentCount();
                                if (0 === usersCount) {
                                    let roles = config.roles;
                                    const initRoles = Object.keys(roles).filter((role) => role !== 'public' && roles[role].initial);
                                    user = await userCreation(data, initRoles);
                                    return {user};
                                } else {
                                    user = await User.findOne(query);
                                    if (authenticate(user)) {
                                        return {user};
                                    }
                                    return { message: config.auth.unauthorizedMessage || "Unauthorized"};
                                }
                            } catch(e) {
                                return { message: e };
                            }
                        },
                    }
                );
                if (middlewareProvider) {
                    middlewareChain.push(middlewareProvider);
                }
                authRouter.use('/'+name, authModuleRouter);
            });
            middlewareChain.reverse(); //so the last one is processed first
            authRouter.get('/success', (req, res, next) => {
                const url = config.auth.success(req.user);
                res.redirect(url);
            });
            authRouter.get('/login', (req, res, next) => {
                let moduleNames = Object.keys(modules);
                if (moduleNames.length > 0) res.redirect('/auth/' + moduleNames[0] + "/login");
            });
            authRouter.get('/logout', (req, res) => {
                req.logout();
                res.redirect('/');
            });
            app.use('/auth', authRouter);

            passport.serializeUser((user, done) => done(null, Generator.map(user)));
            passport.deserializeUser((user, done) => done(null, Generator.map(user)));
        },
        middleware(role) {
            return middlewareChain.reduce((nextAuth, currentProvider) => currentProvider(role, nextAuth), null);
        }
    };
};