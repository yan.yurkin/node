'use strict';
const crypto = require('crypto');

module.exports = {
    genRandomString(length = 16) {
        return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex')
            .slice(0,length);
    }
};