const FacebooStrategy = require('passport-facebook').Strategy;
const successUrl = require('lifeNode/http/security/auth').success;
const name = 'facebook';
const db = require('lifeNode/data/db');

const User = db.models.User;

const self = {
    name,
    async createUser(data, roles) {
      return await User.create({
            auth: { facebook: { facebook_id: data.id } },
            info: [
                {
                    value: data.name,
                    type: 'name'
                },
                {
                    value: data.email,
                    type: 'email'
                }
            ],
            roles,
        });
    },
    /*options: {
        clientID: '99999999999',
        clientSecret: 'vEry_Seqret',
    }*/
    init(passport, options, router, userUtil) {
        passport.use(name, new FacebooStrategy({
                ...options,
                callbackURL:  process.env.APP_URL+`/auth/${name}/callback`,
                profileFields: ["email", "name"],
            },
            async (accessToken, refreshToken, profile, done) => {
                const { id, first_name, last_name } = profile._json;
                const data = {
                    id,
                    email: `${id}@facebook.com`,
                    name: `${first_name} ${last_name}`,
                };
                const result = await userUtil.authOrCreateFirstUser(
                    { auth: { facebook: { facebook_id: id } } },
                    data,
                    (user) => {
                        if (!user) self.createUser(data, []).then();
                        return !!user;
                    },
                    self.createUser
                );
                let user = result.user;
                if (user) {
                    return done(null, user.toObject());
                } else {
                    return done(result.message);
                }
            }
        ));

        // registerEndpoints
        router.get('/login', (...args) => {
            console.log('Trying to authenticate');
            const result = passport.authenticate(name)(...args);
            console.log(result);
            return result;
        });

        // Perform the final stage of authentication and redirect to previously requested URL or '/user'
        router.get('/callback', passport.authenticate(name, { failureRedirect: `/auth/${name}/login` }),
            function(req, res) {
                const returnTo = req.session.returnTo;
                delete req.session.returnTo;
                req.session.save(err => {
                    if (err) return console.error(err);
                    res.redirect(returnTo || successUrl);
                });
            });

        return null; //no additional middleware
    }
};

module.exports = self;