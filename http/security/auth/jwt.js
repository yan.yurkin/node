const jwt = require('jsonwebtoken');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const name = 'jwt';

module.exports = {
    name,
    /*options: {
        key: 's0Me R@nD0M $tR1nG',
        parent: 'local', //parent-strategy name
    }*/
    init(passport, options, router, userUtil) {
        passport.use(new JwtStrategy({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: options.key,
        }, function(jwt_payload, done) {
            const user = jwt_payload;
            if (user) return done(null, user);
            else return done(null, false);

        }));

        // registerEndpoints
        router.post('/token', function (req, res, next) {
            passport.authenticate(options.parent, {session: false}, (err, user, info) => {
                if (err || !user) {
                    return res.status(401).json({message: "Incorrect credentials", err, user});
                }
                req.login(user, {session: false}, (err) => {
                    if (err) {
                        res.send(err);
                    }
                    let payload = {
                        _id: user._$id,
                        ..._.pick(user, ['login', 'role']), //TODO: weird part
                    };
                    res.status(200).json({
                        token: jwt.sign(payload, options.key, { expiresIn: '15m' }),
                        user: _.pick(payload, ['login', 'role']),
                    });
                });
            })(req, res);
        });

        //add middlewareprovider to a chain
        return (role, nextAuth) => (req, res, next) => {
            const client = req.get("Client");
            if (client && ['mobile', 'android'].includes(client)) {
                passport.authenticate('jwt', { session: false })(req, res, () => nextAuth(req, res, next));
            } else {
                nextAuth(req, res, next);
            }
        };

    }
};