const GoogleStrategy = require('@passport-next/passport-google-oauth2').Strategy;
const successUrl = require('lifeNode/http/security/auth').success;
const name = 'google';
const db = require('lifeNode/data/db');

const User = db.models.User;

const self = {
    name,
    /*options: {
        clientID: 'g00G1e-pr0v1ded.apps.googleusercontent.com',
        clientSecret: 'vEry_Seqret',
    }*/
    async createUser(data, roles) {
      return await User.create({
            auth: {
                google: {
                    google_id: data.profile.id,
                },
            },
            info: [
                {
                    value: data.profile.displayName,
                    type: 'name'
                },
                {
                    value: data.email,
                    type: 'email'
                }
            ],
            roles,
        });
    },
    init(passport, options, router, userUtil) {
        passport.use(new GoogleStrategy({
                ...options,
                callbackURL:  process.env.APP_URL+`/auth/${name}/callback`
            },
            async (accessToken, refreshToken, profile, done) => {
                let email = '';
                try {
                    email = profile.emails[0].value;
                } catch (e) {
                    console.error('Cannot get email');
                }
                let data = {
                    profile,
                    email
                };
                const result = await userUtil.authOrCreateFirstUser(
                    {
                        auth: {
                            google: {
                                google_id: profile.id
                            }
                        }
                    },
                    data,
                    (user) => {
                        if (!user) self.createUser(data, []).then();
                        return !!user;
                    },
                    self.createUser
                );
                let user = result.user;
                if (user) {
                    return done(null, user.toObject());
                } else {
                    return done(result.message);
                }
            }
        ));

        // registerEndpoints
        router.get('/login', passport.authenticate('google', {
            scope: [
                'profile',
                'email'
            ],
            prompt: 'consent',
        }));

        // Perform the final stage of authentication and redirect to previously requested URL or '/user'
        router.get('/callback', passport.authenticate('google', { failureRedirect: `/auth/${name}/login` }),
            function(req, res) {
                const returnTo = req.session.returnTo;
                delete req.session.returnTo;
                req.session.save(err => {
                    if (err) return console.error(err);
                    res.redirect(returnTo || successUrl);
                });
            });

        return null; //no additional middleware
    }
};

module.exports = self;