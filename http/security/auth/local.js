const crypto = require('crypto');
const GenUtils = require('../utils/GenUtils');
const successUrl = require('lifeNode/http/security/auth').success;
const LocalStrategy = require('passport-local').Strategy;
const db = require('lifeNode/data/db');

const User = db.models.User;
const defaultSalt = 'htv"]CnYou;%s?Kw{8jR!rzvKrlcDv#2Vr-*;KYFpj-nJ"ssI9SBHcvSSL`ud3n';
const name = 'local';
const saltLength = 16;

const sha512 = (password, salt) => {
    let hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    return hash.digest('hex');
};

const saltHashPassword = (login, password, salt = GenUtils.genRandomString(saltLength)) => {
    const sha = sha512;
    const passwordData = sha(sha(sha(password, login), defaultSalt), salt);
    return {
        hash: passwordData,
        salt: salt
    }
};

const validPassword = (login, password, hash) => {
    return saltHashPassword(login, password, hash.substring(0, saltLength)).hash === hash.substring(saltLength);
};

const getQuery = (username) => {
    return { "auth.local.login": username };
};

const self = {
    name,
    async createUser(data, roles) {
        if (await User.findOne(getQuery(data.username))) throw "Username exists!";
        const hashAndSalt = saltHashPassword(data.username, data.password);
        return await User.create({
            auth: {
                local: {
                    login: data.username,
                    hash: hashAndSalt.salt + hashAndSalt.hash,
                },
            },
            roles
        });
    },
    init(passport, options, router, userUtil) {
        passport.use(new LocalStrategy(
            async (username, password, done) => {
                let result = await userUtil.authOrCreateFirstUser(
                    getQuery(username),
                    {
                        username,
                        password
                    },
                    (user) => !!user && validPassword(username, password, user.auth.local.hash),
                    self.createUser
                );
                let user = result.user;
                if (user) {
                    return done(null, user.toObject());
                } else {
                    return done(null, false, result);
                }
            }
        ));

        // registerEndpoints
        router.get('/login', function(req, res, next) {
            res.render('login',{ //TODO: put login template in template folder of library
                name: 'Login',
            });
        });

        //TODO: One particular day, we will do something
        /*router.get('/signup', function(req, res, next) {
            res.render('login',{
                name: 'Sign Up',
            });
        });*/

        router.post('/verify', passport.authenticate(
            name,
            { successRedirect: successUrl, failureRedirect: `/auth/${name}/login`}
        ));

        return null; //no additional middleware
    },
};

module.exports = self;