require('../essentials');
const debug = require('debug')('www:server');
const http = require('http');
const express = require('express');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
// const passport = require('passport');  //TODO!!!
const mustacheExpress = require('mustache-express');
const loggers = require('./middleware/logger');
const db = require('lifeNode/data/db');
const api = require('./api/rest');

console.log("Starting application in " + process.env.NODE_ENV + " mode");

const normalizePort = val => {
    const port = parseInt(val, 10);
    if (isNaN(port)) return val; // named pipe
    return (port >= 0) ? port : false;
};

const onError = error => {
    if (error.syscall !== 'listen') {
        throw error;
    }
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error({'Some of ports require elevated privileges': error});
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error({'Already in use': error});
            process.exit(1);
            break;
        default:
            throw error;
    }
};

const port = normalizePort(process.env.PORT || '1632');

/****************************
 config = {
    templates: '/templates',
    https: true|false, //if app served over https
    secret: 's0Me R@nD0M $tR1nG', //encryption key for session cookie
    connection: mongoose.connection, //if connection is different from default data/db
    security: {object returned by lifeNode/http/security/config},
    static: {
        'url': 'path'
    },
    worker: app => {
        app.use...
        app.get...
    }
}
 ****************************/

module.exports = (config = {}) => {
    const app = express();
    app.use(helmet());
    loggers.forEach(logger => app.use(logger));

    app.engine('html', mustacheExpress());
    app.set('view engine', 'html');
    app.set('views', config.templates || appRoot+'/templates');

    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    //  ------------- Session
    app.use(cookieParser());
    app.set('trust proxy', 1); //TODO: Config?
    app.use(session({
        secret: config.secret || `IUc6kVBTbpgtreYCm0y5ia4IQolo21NONYMeboSeqK0nC4UqUb16AyCsTL27xpVoyGojMhoh3xV71A7Ca5KZttSGiofvXj4RyMdtBYKinUyAqWudhZ243N4NIwTpA9oTEP7UnteaDjCXxVlDraHxsrqA9M6aJRZK`,
        store: new MongoStore({
            mongooseConnection: config.connection || db.connection,
            touchAfter: 300, //store session on update or each 5 minutes
        }),
        resave: true,
        saveUninitialized: false,
        cookie: {
            maxAge: 1800000,
            secure: config.https,
            httpOnly: true,
        },
        name: 'mars.weather',
        proxy: true,

    }));
    // // if (isDev) { //session debugging
    // //     app.all('*', function (req, res, next) {
    // //         console.log(`${req.method} ${req.session ? req.session.id : 'guest'} - ${req.originalUrl}`);
    // //         next(); // pass control to the next handler
    // //     });
    // // }
    config.security.worker(app);

    if (config.static) {
        Object.keys(config.static).forEach(
            url => app.use(url, express.static(appRoot+config.static[url]))
        )
    }

    config.worker(app);
    app.use(api.prefix, api.router(config.security));

    //the VERY last one - if none matched - redirect to root
    app.all('*', function (req, res, next) {
        if(!res.headersSent) res.status(404).redirect('/');
    });

    app.set('port', port);

    const server = http.createServer(app);
    server.listen(port);
    server.on('error', onError);
    server.on('listening', () => {
        const addr = server.address();
        debug('Listening on ' + (typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port));
    });
};