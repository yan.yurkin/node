'use strict';
const _ = require('lodash');
const pluralize = require('pluralize');
const db = require('lifeNode/data/db');
const _require = require('lifeNode/essentials/require_if_exists');
const VirtualsHelper = require('lifeNode/data/util/VirtualsHelper');
const ObjectId = require('mongoose').Types.ObjectId;

const declaredPreprocessors = _require('@root/http/api/preprocessors', {});
const declaredPostprocessors = _require('@root/http/api/postprocessors', {});

const types = {
    order: 'order',
    add: 'add',
    edit: 'edit',
    delete: 'delete',
};

const preProcessors = {
    empty: function(){},
    get(url, method) {
        const urlHandler = this[url];
        if (!urlHandler) return this.empty;
        return urlHandler[method] || this.empty;
    },
    ...declaredPreprocessors,
};

const postProcessors = {
    empty: function(){},
    get(url, method) {
        const urlHandler = this[url];
        if (!urlHandler) return this.empty;
        return urlHandler[method] || this.empty;
    },
    ...declaredPostprocessors,
};

function toUrl(path) {
    let url = '/' + path.join('/');
    if (url.slice(-3) !== '_id') {
        url += '/';
    }
    return url;
}

const topLevelRestHandler = (method, levels, path, type) =>
    async function(req, res, next) {  //top Documents handler
        try {
            const query = (type !== types.add) ? {_id: req.params[path[1].slice(1)]} : null;
            const doc = (type !== types.delete) ? req.body : null;
            const model = db.models[_.upperFirst(levels[0])];
            if (!model) console.error("Failed to find model: " + _.upperFirst(levels[0]));
            const abort = await preProcessors.get(toUrl(path), method)(req.params, req, doc);
            if (abort) {
                res.status(406).send({
                    error: _.isString(abort) ? `Action Aborted: ${abort}` : 'Action aborted',
                });
                return;
            }
            if (type === types.add) {
                const d = await VirtualsHelper.populateVirtualToObject(model, await model.create(doc)); //todo: do we need to support adding of arrays here?
                res.json(d.toObject());
            } else if (type === types.edit) {
                const found = await model.findOne(query).exec();
                Object.assign(found, doc);
                const d = await VirtualsHelper.populateVirtualToObject(model, await found.save());
                res.json(d.toObject());
            } else if (type === types.delete) {
                const result = await model.deleteOne(query).lean().exec();
                if (result.ok && result.n && result.n === 1) {
                    res.json({ok: result});
                } else {
                    res.sendStatus(404);
                }
            } else throw "Unknown method";
            postProcessors.get(toUrl(path), method)(req.params);
            next();
        } catch (e) {
            console.error(e);
            res.status(410).send(e)
        }
    };

function findInEntity(entity, path, params) {
    let what = entity;
    const last = path.last();
    path.slice(2).forEach(step => {
        if (step[0] === ':') {
            const found = what.id(params[step.slice(1)]);
            if (found) {
                what = found;
            } else throw 'Not found';
        } else {
            what = what[pluralize(step)];
        }
    });
    return what;
}

const subDocRestHandler = (method, levels, path, type) =>
    async function(req, res, next) {  //subDocuments handler
        try {
            const query = {_id: req.params[path[1].slice(1)]};
            const doc = (type !== types.delete) ? req.body : null;
            const modelName = _.upperFirst(levels[0]);
            const model = db.models[modelName];
            const entity = await model.findOne(query).exec();
            let what;
            let typeOverride = type;
            try {
                what = findInEntity(entity, path, req.params);
            } catch (e) {
                const fallback = findInEntity(entity, path.slice(0, -1), req.params);
                if (fallback) {
                    typeOverride = types.add;
                    doc._id = req.params[path.slice(-1)[0].slice(1)] = ObjectId();
                    what = fallback;
                    console.log('Fallback');
                } else {
                    throw 'Not Found';
                }
            }
            const abort = await preProcessors.get(toUrl(path), method)(req.params, req, doc);
            if (abort) {
                res.status(406).send({
                    error: _.isString(abort) ? `Action Aborted: ${abort}` : 'Action aborted',
                });
                return;
            }

            if (typeOverride === types.add) {
                what.push(doc);
                what = what.last();
            } else if (typeOverride === types.edit) {
                what = Object.assign(what, doc);
            } else if (typeOverride === types.delete) {
                try {
                    what.remove();
                    res.json({ok: true});
                } catch (e) {
                    console.error(e);
                    res.status(404).send(e)
                }
            } else throw "Unknown method";
            const savedEntity = await entity.save();
            // what = findInEntity(savedEntity, path, req.params);
            if (!res.headersSent) {
                res.json(what.toObject ? what.toObject() : what);
            }
            postProcessors.get(toUrl(path), method)(req.params);
            next();
        } catch (e) {
            console.error({
                error: e,
                url: req.originalUrl,
                path: path.join('/'),
            });
            res.status(410).send(e)
        }
    };

const reorderingHandler = (method, levels, path, type) =>
    async function(req, res, next) {  //subDocuments handler
        function error(e, code = 500) {
            try {
                console.error(e);
                res.status(code).send(e);
            } catch (err) {
                console.error(err);
            }
        }
        try {
            const query = {_id: req.params[path[1].slice(1)]};
            const doc = req.body;
            const modelName = _.upperFirst(levels[0]);
            const model = db.models[modelName];
            const found = await model.findOne(query).exec();
            if (path.last() !== types.order) return error('reorderingHandler invoked for wrong path');
            if (!_.isFinite(doc.order)) return error('order param is invalid', 410);
            let what = found;
            let list = null;
            path.slice(2, -1).forEach(step => {
                if (step[0] === ':') {
                    const found = what.id(req.params[step.slice(1)]);
                    if (found) {
                        what = found;
                    } else return error("Not found", 404);
                } else {
                    what = what[pluralize(step)];
                    list = what;
                }
            });
            const order = doc.order;
            const src = list.sort((a, b) => a.order - b.order);
            const up = what.order < order; //going up, order is decreasing
            // (($model->order < $order) ? $i->order <= $order : $i->order < $order)
            let target = [
                ...src.filter(e => (up ? e.order <= order : e.order < order) && (e._id !== what._id)),
                what
            ];
            target = [...target, ...src.filter(e => !target.includes(e))];
            let ord = 0;
            target.forEach(e => e.order = ord++);
            await found.save();
            if (!res.headersSent) {
                res.json(list.toObject());
            }
            next();
        } catch (e) {
            console.error(e);
            res.status(410).send(e)
        }
    };


module.exports = (prefix, rolePrefix, roleRouter, entities) => {
    const roleName = rolePrefix.replace('/', '');
    const routeInfo = {};
    entities.forEach(topLevel => {

        const toRoute = function (method, levels, path, type) {
            if (!levels || levels.length === 0) return;
            let url = toUrl(path);
            let handler;
            if (type === types.order) {
                handler = reorderingHandler(method, levels, path, type);
            } else {
                const isTop = levels.length === 1;
                handler = isTop ?
                    topLevelRestHandler(method, levels, path, type) :
                    subDocRestHandler(method, levels, path, type);
            }
            roleRouter[method](url, handler);
            const name = (roleName ? (roleName + '.') : '') + _.camelCase([type, ...levels].join('_'));
            routeInfo[name] = {
                name,
                method,
                url: prefix + rolePrefix + url,
                params: path.filter(s => s[0] === ':').map(s => s.slice(1)),
            };
        };

        const path = function (levels, order = false) {
            const _path = levels.flatMap(l => [l, `:${l}_id`]);
            if (order) {
                toRoute('post', levels, [..._path, types.order], types.order);
            } else {
                toRoute('post', levels, _path.slice(0, -1), types.add);
                toRoute('post', levels, _path, types.edit);
                toRoute('delete', levels, _path, types.delete);
            }
        };

        const parse = function (schema, levels) {
            let keys = Object.keys(schema);
            if (schema.tree) {
                const tree = schema.tree;
                keys = Object.keys(tree);
                keys.filter(key => _.isArrayLike(tree[key]))
                    .forEach(key => {
                        const newLevels = [...levels, pluralize.singular(key)];
                        path(newLevels);
                        parse(tree[key][0], newLevels);
                    });
            }
            keys
                .filter(key => key === types.order)
                .forEach(key => {
                    path(levels, types.order);
                });
        };
        const top = [topLevel.toLocaleLowerCase()];
        path(top);
        parse(db.schemas[topLevel], top);
    });
    return routeInfo;
};

