const _ = require('lodash');
const express = require('express');
const router = express.Router();
const fs = require('fs');
const RestApiGenerator = require('./RestApiGenerator');
const ImageApiGenerator = require('./ImageApiGenerator');

const extractParameters = (url) => {
    return url.split('/')
        .filter(part => part[0] === ':')
        .map(part => part.slice(1))
};
const prefix = '/api';

module.exports = {
    prefix,
    router: security => {
        console.log('Generating REST APIs');
        const roles = security.config.roles;
        let routeInfo = {};
        Object.keys(roles)
            .forEach(role => {
                let roleConfig = roles[role];
                if (!roleConfig.api && !roleConfig.entities) return;
                let roleRouter;
                let rolePrefix;
                if ('public' === role) {
                    roleRouter = router;
                    rolePrefix = '';
                } else {
                    roleRouter = new express.Router();
                    roleRouter.use(security.middleware(role));
                    rolePrefix = '/' + role;
                    router.use(rolePrefix, roleRouter);
                }
                if (roleConfig.entities) {
                    const entities = roleConfig.entities.map(item => _.isString(item) ? item : item.name);
                    routeInfo = {
                        ...routeInfo,
                        ...RestApiGenerator(prefix, rolePrefix, roleRouter, entities),
                        ...ImageApiGenerator(
                            prefix,
                            rolePrefix,
                            roleRouter,
                            roleConfig.entities
                                .map(item => _.isObject(item) ? item : null)
                                .filter(item => !!item)
                        )
                    };
                    if (isDev) {
                        entities.forEach(e => console.log(`    CRUD ${prefix + rolePrefix}/${e}`))
                    }
                }
                if (roleConfig.api) {
                    const api = roleConfig.api;
                    const api_params = roleConfig.api_params || [];
                    api.forEach(endpoint => {
                        const method = endpoint.method || 'get';
                        const url = endpoint.url;
                        if (!url) return;
                        if (isDev) console.log(`    ${method.toUpperCase()} /api${rolePrefix}${url}`);
                        roleRouter[method](url, async (req, res, next) => {
                            const params = {};
                            extractParameters(url).forEach(param => {
                                params[param] = req.params[param];
                            });
                            Object.assign(params, _.pick(req, ['user', ...api_params]));
                            if (method === 'post') params.body = req.body;
                            try {
                                let result = await endpoint.handler(params);
                                if (endpoint.postProcessor) {
                                    result = endpoint.postProcessor(result, res);
                                }
                                if (result === undefined || result === null) {
                                    res.json({});
                                    return;
                                }
                                if (_.isFunction(result.toObject)) result = result.toObject();
                                if (_.isArray(result)) result = result.map(item => _.isFunction(item.toObject) ? item.toObject() : item);
                                res.json(result)
                            } catch (e) {
                                console.error(e);
                                res.status(422)
                                    .send({errors: [e]});
                            }
                        });
                    });
                }
            });
        fs.writeFile('http/routes/backend.json', JSON.stringify(routeInfo, null, 2), 'utf8', err => {
            if (err) console.log(err)
        });
        console.log('REST APIs are ready');
        return router;
    },
};