'use strict';
const _ = require('lodash');
const fs = require('fs');
const mkdirp = require('mkdirp');
const pluralize = require('pluralize');
const multer  = require('multer');
const sharp = require('sharp');
const UUID = require('pure-uuid');
const path = require('path');
const db = require('lifeNode/data/db');

const tmpDir = 'storage/tmp';
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, tmpDir);
    },
    filename: function (req, file, cb) {
        cb(null, new UUID(4).format())
    }
});

const upload = multer({storage});

mkdirp(tmpDir, (err) => {if (err && err.code !== 'EEXIST') console.log(err)});

function resizeCrop(source, filename, size) {
    return sharp(source)
        .trim(40)
        .resize(size, size, {
            fit: 'contain',
            background: '#ffffff',
        })
        .flatten({background:{r:255, g:255, b:255}})
        .toFile(filename);
}

module.exports = (prefix, rolePrefix, router, infos) => {
    const routeInfo = {};
    //const roleName = rolePrefix.replace('/', '');
    infos.forEach(info => {
        const entityName = _.lowerCase(info.name);
        const pathField = info.field;
        mkdirp(  `${appRoot}/storage/public/${entityName}/`, (err) => {
            if (err && err.code !== 'EEXIST') console.log(err)
        });

        function routerPost(field, isAmbiguous) {
            const url = `/upload/${entityName}` + (isAmbiguous ? `/${field}` : '') + '/:id';
            router.post(url, upload.single('file'), async function (req, res, next) {
                const file = req.file;
                const filename = file.filename;
                const source = file.path;
                try {
                    const model = db.models[info.name];
                    const [found] = await Promise.all([
                        model.findById(req.params.id).exec(),
                        resizeCrop(source, `${appRoot}/storage/public/${entityName}/` + filename + '.jpg', info.size),
                    ]);
                    const filePath = `/storage/${entityName}/` + filename + '.jpg';
                    if (!filePath) throw "File path is empty";
                    if (_.isArray(found[field])) {
                        found[field].push(filePath);
                    } else {
                        found[field] = filePath;
                    }
                    await found.save();
                    res.json(found.toObject());
                } catch (e) {
                    console.log(e);
                    res.status(404).send(e);
                }
                fs.unlink(source, delErr => {
                    if (delErr) console.error('Cannot delete temporary file: ' + delErr);
                });
            });
            const name = rolePrefix.replace('/', '')  + `.upload${info.name}${_.upperFirst(_.camelCase(field))}`;
            routeInfo[name] = {
                name,
                method: 'post',
                url: prefix + rolePrefix + url,
                params: url.split('/').filter(s => s[0] === ':').map(s => s.slice(1)),
            };
        }
        if (_.isArray(pathField)) {
            pathField.forEach(field => {
                routerPost(field, true);
            });
        } else {
            routerPost(pathField, false)
        }
    });
    return routeInfo;
};

