const fs = require('fs');
const path = require('path');
const FsUtils = require('lifeNode/essentials/utils/FsUtils');
const logger = require('morgan');

const loggers = [];

loggers.push(logger(':date[clf] :method :url :status :response-time ms - :res[content-length]'));

if (!isDev) {
    const rfs = require('rotating-file-stream');
    const logDirectory = path.join(appRoot, 'log');
    FsUtils.mkDir(logDirectory);
    const accessLogStream = rfs('access.log', {
        interval: '1d', // rotate daily
        path: logDirectory
    });
    loggers.push(logger('combined', { stream: accessLogStream }));
}

module.exports = loggers;