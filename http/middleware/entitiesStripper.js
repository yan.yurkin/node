'use strict';
const mung = require('express-mung');
const visitor = require('../../essentials/utils/visitor');

function process(what) {
    visitor(e => {
        Object.keys(e).forEach(key => {
           if (key.endsWith('_id')) {
               let entity = key.substring(0, key.length - "_id".length);
               if (e[entity]) delete e[entity];
           }
        });
    }).visit(what);
    return what;
}

module.exports = mung.json((body, req, res) => {
    return process(body);
});
