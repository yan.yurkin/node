module.exports = (moduleName, valueIfNot, err) => {
    try {
        return require(moduleName);
    } catch (error) {
        console.warn(err || {'Failed to require module': moduleName, error});
        return valueIfNot;
    }
};