require('dotenv').config();
process.env.NODE_ENV = process.env.APP_ENV || "development";
global.isDev = (process.env.NODE_ENV === 'development');
global.appRoot = require('./appRoot');
const moduleAlias = require('module-alias');
moduleAlias.addAlias('@root', '' + appRoot);
require('module-alias/register');
