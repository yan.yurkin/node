const _ = require('lodash');

module.exports = (action) => ({
    _what(e) {
        return _.isArray(e) ? e : _.isObject(e) ? Object.values(e) : [];
    },
    _visitReversed(arr) {
        return arr
            .filter(e => _.isObject(e) ||_.isArray(e))
            .flatMap(e => {
                if (false !== action(e)) {
                    return this._what(e);
                } else {
                    return [];
                }
            });
    },
    visit(e) {
        if (!_.isArray(e) && _.isObject(e)) {
            if (false === action(e)) return;
        }
        let level = this._what(e);
        while (level.length > 0) {
            level = this._visitReversed(level);
        }
    }
});