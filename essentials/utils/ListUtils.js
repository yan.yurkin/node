module.exports = {
    remove(item, list) {
        let idx = list.indexOf(item);
        let found = -1 !== idx;
        if (found) {
            list.splice(idx, 1);
        }
        return found;
    },
    clear(list) {
        list.splice(0, list.length);
    },
    addAll(list, newValues) {
        for (let itm of newValues) {
            list.push(itm);
        }
    },
    set(list, newValues) {
        this.clear(list);
        this.addAll(list, newValues);
    },
    findById(id, list, kidsFunction) {
        if (id && list) {
            let found = list.find(e => e._id === id);
            if (found) {
                return found;
            } else if (kidsFunction) {
                for (let itm of list) {
                    let kids = kidsFunction(itm);
                    if (kids) {
                        let found = this.findById(id, kids, kidsFunction);
                        if (found) return found;
                    }
                }
            }
            return null;
        }
    },
    findList(item, list, kidsFunction) {
        if (!list) return null;
        let idx = list.indexOf(item);
        if (-1 !== idx) {
            return list;
        } else {
            for (let itm of list) {
                let kids = kidsFunction(itm);
                if (kids) {
                    let found = this.findList(item, kids, kidsFunction);
                    if (found) return found;
                }
            }
        }
        return null;
    },
    replace(list, item) {
        if (item._id) {
            if (!list.findIndex) debugger;
            let idx = list.findIndex(e => e._id === item._id);
            let found = -1 !== idx;
            if (found) {
                list.splice(idx, 1, item);
            }
            return found;
        }
    },
    addOrReplace(list, item) {
        if (!this.replace(list, item)) {
            list.push(item);
        }
    },
    isEmpty(list) {
        return !(list && (list.length > 0));
    },
    default(list, defaultValue) {
        return this.isEmpty(list) ? [defaultValue] : list;
    },
    safe(list) {
        return this.isEmpty(list) ? [] : list;
    },
    sort: { //todo: remove / move to comparators
        numeric (a,b) {
            return a-b;
        },
        alpha (a,b) {
            let _a = a.toLocaleLowerCase();
            let _b = b.toLocaleLowerCase();
            return _a > _b ? 1 :
                _a < _b ? -1 : 0;
        },
        date(a, b) {
            return a > b ? 1 :
                a < b ? -1 : 0;
        }
    },
    min(list) {
        return list.reduce((min, v) => min === null ? v : v < min ? v : min, null);
    },
    max(list) {
        return list.reduce((max, v) => max === null ? v : v > max ? v : max, null);
    },
    equals(array1, array2) {
        return array1 &&
            array2 &&
            array1.length === array2.length &&
            array1.every(e => array2.includes(e))
    },
    includesAll(superset, subset, threshold = 0) {
        const diff = subset.reduce((diff, item)=>diff + (superset.includes(item) ? 0 : 1), 0);
        return superset && subset && (diff <= threshold);
    }
};