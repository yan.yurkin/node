const fs = require('fs');
const mkdirp = require('mkdirp');

function handle(err) {
    if (err && err.code !== 'EEXIST') console.log(err);
}

module.exports = {
    mkDir(path, sync = false) {
        if (fs.existsSync(path)) return;
        if (sync) {
            try {
                mkdirp.sync(path);
            } catch (err) {
                handle(err);
            }
        } else {
            mkdirp(path, handle);
        }
    }
};

