const _ = require('lodash');

module.exports = (action) => ({
    visit(e) {
        // TODO: look for bug of parameter in frontend
        if (_.isArray(e)) {
            e.forEach(el => this.visit(el));
        } else if (_.isObject(e)) {
            if (false !== action(e)) {
                Object.keys(e)
                    .forEach(k => this.visit(e[k]));
            }
        }
    }
});