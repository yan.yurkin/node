
let assign = null;

module.exports = (to, ...from) => {
    if (!assign) {
        try {
            assign = Vue.prototype.$assign;
        } catch (e) {
            assign = Object.assign
        }
    }
    return assign(to, ...from);
};