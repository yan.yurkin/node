
Array.prototype.flatMap = Array.prototype.flatMap || function (mapper) {
    return this.reduce((acc, item)=>acc.concat(mapper(item)), []);
};

Array.prototype.unique = Array.prototype.unique || function () {
    return [...new Set(this)];
};

Array.prototype.last = Array.prototype.last || function () {
    return this.slice(-1)[0];
};

String.prototype.isEmpty = function() {
    return this.trim().length === 0;
};

String.prototype.hashCode = function () {
    return this.split('').reduce((prevHash, currVal) =>
        (((prevHash << 5) - prevHash) + currVal.charCodeAt(0))|0, 0);
};

Object.defineProperty(Object.prototype, '_$id', {
    get: function() {
        return this._id ? (''+ this._id) : '';
    },
    set: function(_id) {
        throw new Error("Cannot set object _$id")
    }
});

__default = (value, defaultValue) => (value === null || value === undefined) ? defaultValue : value;