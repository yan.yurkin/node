const isWebpack = typeof __webpack_require__ === "function";
module.exports = moduleName => {
    if (!isWebpack) { //node environment
        return eval(`require('${moduleName}')`);
    }
};