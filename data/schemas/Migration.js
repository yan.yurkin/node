module.exports = (Schemas, mongoose) => new mongoose.Schema({
    file: String,
    done: {
        type: Boolean,
        default: false,
    },
});