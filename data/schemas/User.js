const infoTypes = ['name', 'email', 'gender', 'age'];

module.exports = (Schemas, mongoose) => new mongoose.Schema({
    auth: {
        facebook: {
            facebook_id: String,
        },
        google: {
            google_id: String,
        },
        local: {
            login: String,
            hash: String,
        },
    },
    info: [{
        value: String,
        type: {
            type: String,
            default: null,
            enum: infoTypes,
        },
    }],
    roles: [ String ],
});