const _ = require('lodash');
const fs = require('fs');
const fieldFilter = require('lifeNode/data/util/fieldFilter');
const ListUtils = require('lifeNode/essentials/utils/ListUtils');
const Generator = require('lifeNode/data/util/Generator');
let oldFields = {};
try {
    oldFields = require(appRoot + '/data/signatures');
} catch (e) {
    console.warn('Failed to find existing data schema (/data/signatures).\n'+e+'\nIs it a first start?')
}

const fields = {};  // contains a list of objects which signature is {schemaName: [fieldsInSchema]}
const differences = [];
let changed = false;

module.exports = db => {
    Object.keys(db.schemas)
        .forEach(schemaName => {
            const schema = db.schemas[schemaName];
            if (schema.tree) {
                const tree = schema.tree;
                const virtuals = Object.keys(schema.virtuals);
                fields[schemaName] = Object.keys(tree).filter(fieldFilter);
                const differ = !ListUtils.equals(fields[schemaName], oldFields[schemaName]);
                if (differ) differences.push(schemaName);
                changed = changed || differ;
            } else {
                console.log('WTF '+schemaName);
            }
        });
    if (changed) {
        console.log("Changed Schemas:");
        differences.forEach(name => console.log("    "+name));
        try {
            fs.writeFileSync(appRoot+'/data/signatures.json', JSON.stringify(fields, null, 2), 'utf8');
            Generator.onSchemasChanged();
        } catch (e) {
            console.warn(e);
        }
    }
};
