'use strict';

let self = {};
self =  {
    async populateVirtualToObject(model, object) {
        let virtuals = model.schema.virtuals;
        return await model.populate(
            object,
            Object.keys(virtuals).filter(item => {
                if ("id" === item) return false; //TODO - if you see this - immediately ask Hengly "Why?"
                const virtual = virtuals[item];
                return virtual.options && virtual.options.foreignField && virtual.options.localField; //only those are auto-populated
            })
        );
    },
    async populateVirtualToObjects(model, objects) {
        const populateModels = objects.map(object => self.populateVirtualToObject(model, object));
        return await Promise.all(populateModels);
    }
};

module.exports = self;