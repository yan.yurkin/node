module.exports = field => !field.startsWith('_') && field !== 'id';
