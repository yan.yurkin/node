const _ = require('lodash');
const BaseData = require('../BaseData');
const fieldFilter = require('./fieldFilter');
const ListUtils = require('../../essentials/utils/ListUtils');
//don't require GeneratorSchema, because it has a dependency on DB and this module should work in both Frontend and Backend
const mapper = {}; // contains a list of object which signature is {schemaName: classForThatSchema}
const uniques = {};

const signaturesPath = '@root/data/signatures.json';

function reloadFields() {
    try {
        const fields = require('@root/data/signatures.json');
        const counters = {};
        Object.values(fields)
            .flatMap(arr => arr)
            .forEach(key => counters[key] = (counters[key] || 0) + 1);
        console.log("Data Classes:");
        return Object.keys(fields)
            .map(schemaName => {
                try {
                    mapper[schemaName] = require(`@root/data/${schemaName}`);
                    uniques[schemaName] = fields[schemaName].filter(field => counters[field] <= 1);
                    console.log("    " + schemaName);
                } catch (e) {/*No data class exis*/
                }
                return schemaName;
            }).map(key => ({
                key,
                value: fields[key]
            }))
            .sort((a, b) => b.value.length - a.value.length);
    } catch (e) {
        console.warn('Failed to find existing data schema (/data/signatures).\nIs it a first start?');
        return [];
    }
}

let fields = reloadFields();  // contains a list of objects which signature is {schemaName: [fieldsInSchema]}

function looksLike(schemaFields, dataObjectFields) {
    const schemaSize = schemaFields.length;
    const threshold = Math.min(schemaSize * 0.6, 5);
    return (dataObjectFields.length >= threshold) && ListUtils.includesAll(schemaFields, dataObjectFields, schemaSize * .2)
}

module.exports = {
    map(dataObject) {
        if (dataObject instanceof BaseData || dataObject.__generator_ignore) return dataObject;
        const dataObjectFields = Object.keys(dataObject).filter(fieldFilter);
        const found = fields.find(item => {
            const uniqueFields = uniques[item.key] || [];
            const uSize = uniqueFields.length;
            const uniquesDetected = uSize >= 3 && _.intersection(dataObjectFields, uniqueFields).length >= Math.floor(uSize / 2);
            return uniquesDetected || looksLike(item.value, dataObjectFields)
        });
        if (found) {
            const cls = mapper[found.key];
            if (cls) {
                // console.log(`pojo => ${found.key}`);
                return new cls(dataObject);
            } else {
                dataObject.__generator_ignore = true; //todo: DRY
                return dataObject;
            }
        }
        // console.warn({'No class found for':dataObject});
        dataObject.__generator_ignore = true;
        return dataObject;
    },
    visit(data) {
        if (!data || data.__generator_ignore) return data;
        if (_.isArray(data)) {
            return data.map(el => this.visit(el));
        } else if (_.isPlainObject(data)) {
            const c = this.map(data);
            Object.keys(c)
                .filter(k => k !== 'translations')
                .forEach(k => c[k] = this.visit(c[k]));
            return c;
        }
        return data;
    },
    onSchemasChanged() {
        delete require.cache[require.resolve('@root/data/signatures.json')];
        fields = reloadFields();
        console.log("Schema signatures are reloaded");
    }
};
