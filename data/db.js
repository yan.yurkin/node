'use strict';
const fs = require('fs');
const path = require('path');
require('lifeNode/essentials');
const FsUtils = require('lifeNode/essentials/utils/FsUtils');
const _require = require('lifeNode/essentials/require_if_exists');
const mongoose = require('mongoose');
const _ = require('lodash');
const GeneratorSchema = require('./GeneratorSchema');

const db = {
    mongoose,
    schemas: {},
    models: {},
    onLoad: [],
};

const dbPath = path.join(appRoot, 'db');
const modelsPath = path.join(dbPath, 'models');
const migrationsPath = path.join(dbPath, 'migrations');

const load = (what, basePath = modelsPath) => {
    let name = what.name || what;
    if (what.dependsOn) {
        what.dependsOn.forEach(d => load(d));
    }
    if (!db.schemas[name]) {
        try {
            const schema = require(path.join(basePath, name))(db.schemas, mongoose);
            db.schemas[name] = schema;
            db.models[name] = mongoose.model(name, schema);
            if (isDev) console.log("    " + name);
        } catch (e) {
            db.schemas[name] = name;
            console.error("FAILED schema: " + name + ", e: " + e);
        }
    }

};

const loadDir = path => {
    fs
        .readdirSync(path)
        .filter(file => {
            return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
        })
        .forEach(file => {
            load(file.slice(0, -3), path);
        });
};

/**************************
 module.exports = relations = [
 {
     name: 'Product',
     dependsOn: [
         { name: 'ParameterValue' },
         {
             name: 'Version',
             dependsOn: [
                 { name: 'ParameterValue' },
             ]
         },
     ]
 },
 .....
 ]
 */
let relations = _require(path.join(dbPath, 'relations'), [], 'No relations defined in db/relations, proceeding with flat models');

FsUtils.mkDir(modelsPath, true);
FsUtils.mkDir(migrationsPath, true);
console.log("Loading model Schemas...");
//load library models
loadDir(path.join(__dirname, 'schemas'));

// ... then process relations first
relations.forEach(r => {
    load(r);
});
// ...and  then all other models
loadDir(modelsPath);

//data classes
GeneratorSchema(db);

const migrate = () => {
    fs  .readdirSync(migrationsPath)
        .filter(file => {
            return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
        })
        .forEach(async migration => {
            let dbMigration = await db.models.Migration.findOne({file: migration}).exec();
            if (!dbMigration || !dbMigration.done) {
                console.log(`Migrating ${migration}`);
                const completed = await require(path.join(migrationsPath, migration))(db);
                if (completed) {
                    if (!dbMigration) {
                        dbMigration = new db.models.Migration({file: migration});
                    }
                    dbMigration.done = true;
                    dbMigration.save()
                }
            }
        });
};

mongoose.set('useUnifiedTopology', true);
mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useFindAndModify: false,
});
const mng = mongoose.connection;
mng.on('error', console.error.bind(console, 'connection error:'));
mng.once('open', async function () {
    console.log("Mongo connected...");
    //migrations
    try {
        const pm_id = process.env.pm_id;
        if (!pm_id) {
            console.log(`DB: Migration: Looks like we're out of PM2`);
            migrate();
        } else {
            {/*
                const pm2 = require('pm2');
                pm2.connect(() => {
                    pm2.list((err, processes) => {
                        const min_id = processes
                            //todo: filter by path
                            .map(process => process.pm_id)
                            .reduce((min, v) => min === null ? v : v < min ? v : min, null);
                        console.log({min_id});
                        if (pm_id == min_id) { //comparing strings with numbers
                            console.log(`We're the first process, migrating`);
                            migrate();
                        } else {
                            console.log({name: process.env.name, id: process.env.pm_id, migration: "skipped"});
                        }
                    });
                });
            */}
            if (pm_id === '0') { //comparing strings with numbers
                console.log({
                    "PM2 detected! Process ID": process.env.pm_id,
                    name: process.env.name,
                    migration: "MIGRATING"
                });
                migrate();
            } else {
                console.log({
                    "PM2 detected! Process ID": process.env.pm_id,
                    name: process.env.name,
                    migration: "skipped"
                });
            }
        }
    } catch (e) {
        console.log({"DB: Migration: Looks like we're out of PM2":e});
        migrate();
    }

    //listeners
    db.onLoad
        .filter(f => _.isFunction(f))
        .forEach(f => f(db));
    
});
db.connection = mng;
module.exports = db;